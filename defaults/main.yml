---
# Copyright (C) 2020-2021 CipherMail B.V. <https://www.ciphermail.com/>
# Copyright (C) 2020-2021 DebOps <https://debops.org/>
# SPDX-License-Identifier: GPL-3.0-or-later

restic__base_packages: '{{ [] if restic__upstream else ["restic"] }}'
restic__packages: []

restic__upstream: '{{ restic__set_capabilities }}'
restic__src: 'github.com/restic/restic'
restic__git_repo: 'https://{{ restic__src }}.git'
restic__version: '{{ "v0.9.6"
                     if ansible_distribution_release in
                        ["stretch", "xenial", "bionic"]
                     else "v0.14.0" }}'
restic__release_keys:
  - id: 'CF8F 18F2 8445 7597 3F79  D4E1 91A6 868B D3F7 A907'
    state: 'present'

restic__golang_user: '_golang'
restic__golang_group: '{{ restic__golang_user }}'
restic__golang_home: '{{ ansible_local.fhs.home | d("/var/local")
                         + "/" + restic__golang_user }}'

restic__user: 'restic'
restic__group: '{{ restic__user }}'
restic__home: '{{ ansible_local.fhs.home | d("/var/local")
                  + "/" + restic__user }}'
restic__shell: '/bin/bash'
restic__comment: 'Restic backup software'

restic__set_capabilities: false

restic__master_password: '{{
  lookup("password", secret + "/restic/master_password"
                     + " chars=ascii_letters,digits length=22") }}'
restic__host_password: '{{ restic__master_password
                           | password_hash("pbkdf2_sha256",
                                           salt=inventory_hostname,
                                           rounds=10000)
                           | regex_replace("^.*\$", "") }}'

restic__url: '/var/cache/restic'
restic__path: '/'

restic__default_environment:
  RESTIC_PASSWORD: '{{ restic__host_password }}'
  RESTIC_REPOSITORY: '{{ restic__url }}'

restic__environment: {}
restic__group_environment: {}
restic__host_environment: {}

restic__combined_environment: '{{ restic__default_environment
                                  | combine(restic__environment,
                                            restic__group_environment,
                                            restic__host_environment) }}'

restic__backup_oncalendar: 'hourly'
restic__backup_randomizeddelaysec: '3600'
restic__onfailure: ''

# Remove old backups at least once every two weeks
restic__forget_oncalendar: '*-*-1,15 00:00:00'
restic__forget_randomizeddelaysec: '1209600'

restic__pre_backup_script: |
  #!/bin/bash
  exit 0

restic__global_flags: ''
restic__backup_flags: '--exclude-file=/etc/restic/exclude'
restic__forget_flags: >-
  --prune --keep-hourly 24 --keep-daily 14 --keep-weekly 11

restic__default_exclude:
  - '/boot'
  - '/dev'
  - '/media'
  - '/mnt'
  - '/proc'
  - '/run'
  - '/sys'
  - '/tmp'
  - '/usr'
  - '/var/cache'
  - '/var/log/lastlog'
  - '/var/tmp'

restic__exclude: []
restic__group_exclude: []
restic__host_exclude: []
restic__combined_exclude: '{{ restic__default_exclude
                              + restic__exclude
                              + restic__group_exclude
                              + restic__host_exclude }}'

restic__keyring__dependent_gpg_user: '{{ restic__golang_user }}'

restic__keyring__dependent_gpg_keys:

  - user: '{{ restic__golang_user }}'
    group: '{{ restic__golang_group }}'
    home: '{{ restic__golang_home }}'
    state: 'present'

  - '{{ q("flattened", restic__golang__dependent_packages) | parse_kv_items
        | selectattr("gpg", "defined")
        | selectattr("state", "equalto", "present")
        | map(attribute="gpg") | list }}'

restic__golang__dependent_packages:

  - name: 'restic'
    state: 'present'
    gpg: '{{ restic__release_keys }}'
    git:
      - repo: '{{ restic__git_repo }}'
        dest: '{{ restic__src }}'
        version: '{{ restic__version }}'
        build_script: |
          git verify-tag {{ restic__version }}
          go run build.go
